<?php

function solicita_su_token() {

?>
<div id='datos_cliente' style="width: 80%;" class="text-center p-2">
<form class="text-center p-2"  id="form_token_decodificar"  title="Ingresar Token" method="post" onSubmit="return decodifica_su_token(num_empleado.value, password.value,clave_terminal.value);">
            <h4 class="mb-4">Terminal de Autobuses</h4>
            <hr class="my-2">
            
            <p class="font-weight-bold">Obten tus recibos de nómina</p>

            <div class="row">
                <div class="d-flex align-items-end mb-4" style="width: 85%;">
                    <input type="number" id="clave_terminal" class="form-control form-control-sm" required="required"step="any" min="1" placeholder="Clave de Terminal" alt="Clave de Terminal"/>
                </div>
                <div class="d-flex align-items-end mb-4 justify-content-center"  style="width: 15%;">
                    <i class="fas fa-building fa-2x text-secondary"></i>
                </div>
            </div>

            <div class="row">
                <div class="d-flex align-items-end mb-4" style="width: 85%;">
                    <input type="number" id="num_empleado" name="num_empleado" class="form-control form-control-sm" required="required" step="any" min="1"  placeholder="Número de Empleado" alt="Capturar Núm. Empleado" />
                </div>
                <div class="d-flex align-items-end mb-4 justify-content-center"  style="width: 15%;">
                    <i class="fas fa-user fa-2x text-secondary"></i>
                </div>
            </div>

            <div class="row">
                <div class="d-flex align-items-end mb-4" style="width: 85%;">
                <input type="password" id="password" name="password" class="form-control form-control-sm" required="required" value=""  min="10" placeholder="Contraseña" alt="Contraseña" />
                </div>
                <div class="d-flex align-items-end mb-4 justify-content-center"  style="width: 15%;">
                <i class="fas fa-asterisk fa-2x text-secondary"></i>
                </div>
            </div>

            <div class="row">
                <div class="d-flex align-items-end mb-4" style="width: 100%;">
                    <button class="btn btn-dark btn-lg btn-block" type="submit">Buscar</button>
                </div>
            </div>
</form>

</div>
<?php
}

function comprimirYDescargarZIP($archivoOrigen, $archivoDestino) {
    $tmp_archivoOrigen = $archivoOrigen;
    $tmp_archivoDestino = $archivoDestino;
    $archivoOrigen = '../storage/uploads/' .$archivoOrigen;
    $archivoDestino = '../storage/zip/'  .$archivoDestino;

    //--echo "Archivo Origen: " .$archivoOrigen ."<br>";
    //--echo "Archivo Destino: " .$archivoDestino ."<br>";

    $link_zip = "";

    if (file_exists($archivoDestino)) {
        //--echo 'El archivo ZIP SI existe.' ."<br>";
        $link_zip = $tmp_archivoDestino;
    } else {
        //--echo 'El archivo ZIP NO existe.' ."<br>";

        if (file_exists($archivoOrigen .".pdf")) {
            //--echo 'El archivo PDF/XML SI existe.' ."<br>";
            $zip = new ZipArchive();

            // Intentar abrir el archivo ZIP en modo temporal
            if ($zip->open($archivoDestino, ZipArchive::CREATE | ZipArchive::OVERWRITE) === TRUE) {
                //--echo "Entre a ZipArchive" ."<br>";

                 // Agregar archivo al ZIP
                $zip->addFile($archivoOrigen .".pdf" , basename($archivoOrigen .".pdf"));
                $zip->addFile($archivoOrigen .".xml" , basename($archivoOrigen .".xml"));
                $zip->close();
                //--echo "Comprimí el PDF y XML en ZIP" ."<br>";

                $link_zip = $tmp_archivoDestino;
            } else {
                //--echo 'Error al crear el archivo ZIP.';
                $link_zip = "";
            }
        } else {
            //--echo 'El archivo PDF/XML NO existe.' ."<br>";
            $link_zip = "";
        }

    }

    if ($link_zip <> ""){
        // echo '<a href="storage/zip/' .$link_zip  .'">LINK DESCARGAR</a>' .'<br>';
        // return '<a href="storage/zip/' .$link_zip  .'">Descargar</a>' .'<br>';
        // return '<a href="storage/zip/' .$link_zip  .'">' .'<img src="assets/imgs/zip_file.png" alt="" width="30" height="40"/>' .'</a>' .'<br>'
        //return '<button id="' .$tmp_archivoOrigen  .'" onclick="callPHPFunction(this.id)">Realizar Tarea</button>';

        return '<img id="' .$tmp_archivoOrigen  .'" src="assets/imgs/zip_file.png" alt="" width="30" height="40" style="cursor: pointer;" onclick="callPHPFunction(this.id)">';

    } else {
        // echo '<a>NO LOCALIZADO</a>' .'<br>';
        return '<a> ----- </a>';
    }
}



function quincenaActual(){
    $numero_mes_actual = date('n');
    $dia_actual = date('d');
    $quincena = 0;

    if ($dia_actual > 15) {
        $quincena = (($numero_mes_actual -1) *2)+1;
    } else {
        $quincena =(($numero_mes_actual -1) *2);
    }
    return $quincena;
}

function semanaActual(){
    return date('W')-1;
}

function Colorear($boton_id) {
    // echo 'Algo' .$boton_id;
    // echo "<script>alert('Tu mensaje aquí');</script>";
}





?>