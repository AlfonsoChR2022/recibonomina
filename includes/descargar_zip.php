<?php

// Llama a la función si se realiza una solicitud GET
if ($_SERVER["REQUEST_METHOD"] == "GET") {
    realizarTarea();
}

// Aquí puedes definir tu función de PHP
function realizarTarea() {
// Recibir el parámetro buttonId enviado desde la solicitud GET
$buttonId = isset($_GET['buttonId']) ? $_GET['buttonId'] : '';
echo "La tarea se ha realizado correctamente para el botón con ID: " . $buttonId;


// ========================================================================================//
// Ruta al archivo que deseas comprimir
//$archivo_a_comprimir = '../storage/uploads/foto.jpg';

// Crear un nuevo objeto de archivo zip
$zip = new ZipArchive();

// Nombre del archivo temporal
//$tempZipFile = tempnam(sys_get_temp_dir(), 'temp_zip');
$tempZipFile = tempnam(sys_get_temp_dir(), 'temp_zip') ;

echo 'tempZipFile....';echo '<br>';


// Obtener el directorio temporal del sistema
$tempDir = sys_get_temp_dir();
echo "Directorio temporal: $tempDir <br>";

echo "tempZipFile: $tempZipFile <br>";


// Abrir el archivo zip temporal en modo escritura
if ($zip->open($tempZipFile, ZipArchive::CREATE | ZipArchive::OVERWRITE) === TRUE) {
    echo 'Open....';echo '<br>';

    // Agregar el archivo a comprimir al archivo ZIP
    $archivo_a_comprimir = '../storage/uploads/' .$buttonId .'.pdf';
    $zip->addFile($archivo_a_comprimir, basename($archivo_a_comprimir));

    $archivo_a_comprimir = '../storage/uploads/' .$buttonId .'.xml';
    $zip->addFile($archivo_a_comprimir, basename($archivo_a_comprimir));


    echo 'addFile....';echo '<br>';

    // Cerrar el archivo ZIP
    $zip->close();
    echo 'close....';echo '<br>';
    
} else {
    echo 'Error: No se pudo crear el archivo ZIP.';
    //exit;
}


// Establecer las cabeceras para la descarga del archivo ZIP
header('Content-Type: application/zip');
header('Content-Disposition: attachment; filename="archivo_comprimido.zip"');
header('Content-Length: ' . filesize($tempZipFile));



echo 'header....';echo '<br>';
echo filesize($tempZipFile) .'<br>';


// Leer el contenido del archivo y enviarlo al cliente
if (ob_get_level()) {
    ob_end_clean();
}



// Leer el contenido del archivo ZIP temporal y enviarlo al cliente
readfile($tempZipFile);
echo 'readfile....';echo '<br>';

// Eliminar el archivo ZIP temporal después de descargarlo
//unlink($tempZipFile);
//echo 'unlink....';echo '<br>';


}




?>
