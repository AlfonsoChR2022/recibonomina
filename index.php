<!DOCTYPE html>
<?php
    session_start(); // Iniciar sesión al principio del archivo
    include('includes/config.php');//para conectar a la base de datos
    include("includes/funciones.php");

    $querysql = "SELECT descrip FROM datempre";
    $result = $db_D->query($querysql);
    $numResults = $result->num_rows;
    if($reg=$result->fetch_assoc())
    {
        //datos de la terminal que factura
        $razon_social_emisor=$reg['descrip'];
    }else{
        $razon_social_emisor ='';
    }
?>

<html lang="es-ES" ><!--<![endif]-->
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <!-- <link rel="stylesheet" href="assets/diseño.css"> -->

    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" rel="stylesheet">

    <script type="text/javascript" src="assets/ajax.js" charset="utf-8"></script>
    <title>Recibo de Nomina </title>

    <script>
    function callPHPFunction(buttonId)  {
        // Crear un objeto XMLHttpRequest
        var xhr = new XMLHttpRequest();

        // Definir la función de respuesta
        xhr.onreadystatechange = function() {
            if (xhr.readyState == 4 && xhr.status == 200) {
                // Crear un objeto Blob con la respuesta del servidor
                var blob = new Blob([xhr.response], { type: "application/zip" });

                // Crear un objeto URL a partir del Blob
                var url = window.URL.createObjectURL(blob);

                // Crear un enlace invisible para descargar el archivo
                var a = document.createElement("a");
                a.href = url;
                a.download = "archivo_comprimido.zip";
                document.body.appendChild(a);
                
                // Hacer clic en el enlace para iniciar la descarga
                a.click();

                // Liberar el objeto URL y eliminar el enlace
                window.URL.revokeObjectURL(url);
                document.body.removeChild(a);
            }
        };

        // Concatenar el buttonId a la URL del archivo PHP
        var url = "includes/descargar_zip.php?buttonId=" + buttonId;

        // Hacer una solicitud al archivo PHP que contiene la función
        xhr.open("GET", url, true);
        xhr.responseType = "arraybuffer"; // Esperamos una respuesta como un ArrayBuffer (para archivos binarios)
        xhr.send();
    }
</script>
</head>

<body id='cuerpo'>

<section id="lolito" class="vh-100" style="background-color: #F0F0F0;">
    <div class="container py-5 h-100">
        <div class="row d-flex justify-content-center align-items-center h-100">
            <div class="col-12 col-md-8 col-lg-6 col-xl-5">
                <div class="card shadow-2-strong" style="border-radius: 1rem">
                    <div class="card p-2 align-items-center" style="width:100%; height:370px">
                        <?php
                            solicita_su_token();
                        ?>
                        <div id='datos_token' class="text-center mx-auto w-100 bg-danger ">
                        <?php
                            // session_start();
                            if(isset($_SESSION['parametro'])) {
                                $parametro = $_SESSION['parametro'];
                                echo $parametro;
                                $parametro ="";
                            }
                            unset($_SESSION['parametro']);
                        ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

</body>
</html>
